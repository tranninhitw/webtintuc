<?php $__env->startSection('content'); ?>
<div class="page-title">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ol class="breadcrumb">
                    <li><a href="#">Trang Chủ</a></li>
                    <li><?php echo e($listnew->listname); ?></li>
                </ol>
            </div>
            <!-- Col end -->
        </div>
        <!-- Row end -->
    </div>
    <!-- Container end -->
</div>
<!-- Page title end -->
<section class="block-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-12">
                <div class="block category-listing category-style2">
                    <h3 class="block-title"><span><?php echo e($listnew->listname); ?></span></h3>
                    <?php 
                    $item = $listnew->most_news_in_list_new($listnew->id);
                    $hot = $item->shift();								
                    ?>
                    <div class="post-block-style post-list clearfix">
                        <div class="row">
                            <div class="col-lg-5 col-md-6">
                                <div class="post-thumb thumb-float-style">
                                    <a href="<?php echo e(url('chi-tiet/'.$hot['slug'])); ?>">
                                    <img class="img-fluid" src="<?php echo e(url('public/img/news/300x300/'.$hot['newimg'])); ?>" alt="" />
                                    </a>
                                    <a class="post-cat" href="#">Đô thị</a>
                                </div>
                            </div>
                            <!-- Img thumb col end -->
                            <div class="col-lg-7 col-md-6">
                                <div class="post-content">
                                    <h2 class="post-title title-large">
                                        <a href="<?php echo e(url('chi-tiet/'.$hot['slug'])); ?>"><?php echo e($hot['newsname']); ?></a>
                                    </h2>
                                    <div class="post-meta">
                                        <span class="post-author"><a href="#"><?php echo e($hot->newuser); ?></a></span>
                                        <span class="post-date"><?php echo e($hot->created_at); ?></span>
                                        <span data-href="<?php echo e(url()->current()); ?> class="post-comment pull-right"> <i class="fa fa-comments-o"></i>
                                        <a href="#" class="comments-link"><span>03</span></a></span>
                                    </div>
                                    <p><?php echo $hot['newintro']; ?></p>
                                </div>
                                <!-- Post content end -->
                            </div>
                            <!-- Post col end -->
                        </div>
                        <!-- 1st row end -->
                    </div>
                    <!-- 1st Post list end -->
                   
                </div>
                <!-- Block Technology end -->
                <div class="paging">
                    <ul class="pagination">
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">»</a></li>
                        <li>
                            <span class="page-numbers">Page 1 of 2</span>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Content Col end -->
            <?php echo $__env->make('home.sitebar_right2', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <!-- Sidebar Col end -->
        </div>
        <!-- Row end -->
    </div>
    <!-- Container end -->
</section>
<!-- First block end -->
<!-- First block end -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('home.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>