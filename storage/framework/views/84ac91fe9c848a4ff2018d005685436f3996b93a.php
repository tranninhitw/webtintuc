<!DOCTYPE html>
<html lang="en">

<head>

	<!-- Basic Page Needs
		================================================== -->
		<meta charset="utf-8">
		<title>Cổng thông tin - tin tức cập nhật mỗi ngày</title>

	<!-- Mobile Specific Metas
		================================================== -->

		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

		<!--Favicon-->
		<link rel="shortcut icon" href="<?php echo e(url('public/frontend/images/favicon.ico')); ?>" type="image/x-icon">
		<link rel="icon" href="<?php echo e(url('public/frontend/images/favicon.ico')); ?>" type="image/x-icon">

	<!-- CSS
		================================================== -->

		<!-- Bootstrap -->
		<link rel="stylesheet" href="<?php echo e(url('public/frontend/css/bootstrap.min.css')); ?>">
		<!-- Template styles-->
		<link rel="stylesheet" href="<?php echo e(url('public/frontend/css/style.css')); ?>">
		<!-- Responsive styles-->
		<link rel="stylesheet" href="<?php echo e(url('public/frontend/css/responsive.css')); ?>">
		<!-- FontAwesome -->
		<link rel="stylesheet" href="<?php echo e(url('public/frontend/css/font-awesome.min.css')); ?>">
		<!-- Animation -->
		<link rel="stylesheet" href="<?php echo e(url('public/frontend/css/animate.css')); ?>">
		<!-- Owl Carousel -->
		<link rel="stylesheet" href="<?php echo e(url('public/frontend/css/owl.carousel.min.css')); ?>">
		<link rel="stylesheet" href="<?php echo e(url('public/frontend/css/owl.theme.default.min.css')); ?>">
		<!-- Colorbox -->
		<link rel="stylesheet" href="<?php echo e(url('public/frontend/css/colorbox.css')); ?>">
</head>
	
<body>

	<div class="body-inner">

		<?php echo $__env->make('components.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

		<?php echo $__env->yieldContent('content'); ?>

		<?php echo $__env->make('components.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	<!-- Javascript Files
		================================================== -->

		<!-- initialize jQuery Library -->
		<script type="text/javascript" src="<?php echo e(url('public/frontend/js/jquery.js')); ?>"></script>
		<!-- Popper js -->
		<script type="text/javascript" src="<?php echo e(url('public/frontend/js/popper.min.js')); ?>"></script>
		<!-- Bootstrap jQuery -->
		<script type="text/javascript" src="<?php echo e(url('public/frontend/js/bootstrap.min.js')); ?>"></script>
		<!-- Owl Carousel -->
		<script type="text/javascript" src="<?php echo e(url('public/frontend/js/owl.carousel.min.js')); ?>"></script>
		<!-- Color box -->
		<script type="text/javascript" src="<?php echo e(url('public/frontend/js/jquery.colorbox.js')); ?>"></script>
		<!-- Smoothscroll -->
		<script type="text/javascript" src="<?php echo e(url('public/frontend/js/smoothscroll.js')); ?>"></script>


		<!-- Template custom -->
		<script type="text/javascript" src="<?php echo e(url('public/frontend/js/custom.js')); ?>"></script>

	</div><!-- Body inner end -->
</body>

</html>