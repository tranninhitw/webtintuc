@extends('home.master2')
@section('content')
<div class="page-title">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ol class="breadcrumb">
                    <li><a href="#">Trang Chủ</a></li>
                    <li>{{ $listnew->listname }}</li>
                </ol>
            </div>
            <!-- Col end -->
        </div>
        <!-- Row end -->
    </div>
    <!-- Container end -->
</div>
<!-- Page title end -->
<section class="block-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-12">
                <div class="block category-listing category-style2">
                    <h3 class="block-title"><span>{{ $listnew->listname }}</span></h3>
                    <?php 
                    $item = $listnew->most_news_in_list_new($listnew->id);
                    $hot = $item->shift();								
                    ?>
                    <div class="post-block-style post-list clearfix">
                        <div class="row">
                            <div class="col-lg-5 col-md-6">
                                <div class="post-thumb thumb-float-style">
                                    <a href="{{url('chi-tiet/'.$hot['slug'])}}">
                                    <img class="img-fluid" src="{{url('public/img/news/300x300/'.$hot['newimg'])}}" alt="" />
                                    </a>
                                    <a class="post-cat" href="#">Đô thị</a>
                                </div>
                            </div>
                            <!-- Img thumb col end -->
                            <div class="col-lg-7 col-md-6">
                                <div class="post-content">
                                    <h2 class="post-title title-large">
                                        <a href="{{url('chi-tiet/'.$hot['slug'])}}">{{$hot['newsname']}}</a>
                                    </h2>
                                    <div class="post-meta">
                                        <span class="post-author"><a href="#">{{$hot->newuser}}</a></span>
                                        <span class="post-date">{{ $hot->created_at }}</span>
                                        <span data-href="{{url()->current()}} class="post-comment pull-right"> <i class="fa fa-comments-o"></i>
                                        <a href="#" class="comments-link"><span>03</span></a></span>
                                    </div>
                                    <p>{!! $hot['newintro'] !!}</p>
                                </div>
                                <!-- Post content end -->
                            </div>
                            <!-- Post col end -->
                        </div>
                        <!-- 1st row end -->
                    </div>
                    <!-- 1st Post list end -->
                   
                </div>
                <!-- Block Technology end -->
                <div class="paging">
                    <ul class="pagination">
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">»</a></li>
                        <li>
                            <span class="page-numbers">Page 1 of 2</span>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Content Col end -->
            @include('home.sitebar_right2')
            <!-- Sidebar Col end -->
        </div>
        <!-- Row end -->
    </div>
    <!-- Container end -->
</section>
<!-- First block end -->
<!-- First block end -->
@stop