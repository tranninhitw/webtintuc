<div class="col-lg-4 col-md-12">
    <div class="sidebar sidebar-right">
        <div class="widget">
            <h3 class="block-title"><span>Theo dõi chúng tôi</span></h3>
            <ul class="social-icon">
                <li><a href="#" target="_blank"><i class="fa fa-rss"></i></a></li>
                <li><a href="#" target="_blank"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                <li><a href="#" target="_blank"><i class="fa fa-vimeo-square"></i></a></li>
                <li><a href="#" target="_blank"><i class="fa fa-youtube"></i></a></li>
            </ul>
        </div>
        <!-- Widget Social end -->
        <div class="widget color-default">
            <h3 class="block-title"><span>Tin mới nhất</span></h3>
            <?php $count =0; ?>
            @foreach($lasted_news as $item_lt)
            @if($count <5)
            <div class="list-post-block">
                <ul class="list-post">
                    <li class="clearfix">
                        <div class="post-block-style post-float clearfix">
                            <div class="post-thumb">
                                <a href="{{url('chi-tiet/'.$item_lt->slug)}}">
                                    <img class="img-fluid" src="{{url('/public/img/news/100x100/'.$item_lt->newimg)}}" alt="" />
                                </a>
                                <a class="post-cat" href="{{url('chi-tiet/'.$item_lt->slug)}}">Hot</a>
                            </div>
                            <!-- Post thumb end -->
                            <div class="post-content">
                                <h2 class="post-title title-small">
                                    <a href="{{url('chi-tiet/'.$item_lt->slug)}}">{{$item_lt->newsname}}</a>
                                </h2>
                                <div class="post-meta">
                                    <span class="post-date">{{$item_lt->created_at}}</span>
                                </div>
                            </div>
                            <!-- Post content end -->
                        </div>
                        <!-- Post block style end -->
                    </li>
                    <!-- Li 4 end -->
                </ul>
                <!-- List post end -->
            </div>
            @endif
            <?php  $count = $count +1; ?>
            @endforeach
            <!-- List post block end -->
        </div>
        <!-- Popular news widget end -->
        <div class="widget text-center">
           @if($adverts_bottom[0]->code != "")
           {{$adverts_bottom[0]->code}}
           @else
           <a href="{{$adverts_bottom[0]->link}}">
              <img src="{{url('public/img/images_bn/'.$adverts_bottom[0]->img)}}" alt="No image" />
          </a>
          @endif
      </div>
      <!-- Sidebar Ad end -->
      <div class="widget color-default m-bottom-0">
        <h3 class="block-title"><span>Đọc nhiều nhất</span></h3>
        <div id="post-slide" class="owl-carousel owl-theme post-slide">
           <?php $count =1; ?>
           @foreach($most_news as $item_most)
           @if($count <6)
           <div class="item">
            <div class="post-overaly-style text-center clearfix">
                <div class="post-thumb">
                    <a href="{{url('chi-tiet/'.$item_most->slug)}}">
                        <img class="img-fluid" src="{{url('/public/img/news/100x100/'.$item_most->newimg)}}" alt="" />
                    </a>
                </div>
                <!-- Post thumb end -->
                <div class="post-content">
                    <a class="post-cat" href="#">Gadgets</a>
                    <h2 class="post-title">
                        <a href="{{url('chi-tiet/'.$item_most->slug)}}">{{$item_most->newsname}}</a>
                    </h2>
                    <div class="post-meta">
                        <span class="post-date">{{$item_most->created_at}}</span>
                    </div>
                </div>
                <!-- Post content end -->
            </div>
        </div>
        @endif
        <?php  $count = $count +1; ?>
        @endforeach
        <!-- Item 2 end -->
    </div>
    <!-- Post slide carousel end -->
</div>
<hr>
<div class="widget text-center">
   @if($adverts_bottom[1]->code != "")
   {{$adverts_bottom[1]->code}}
   @else
   <a href="{{$adverts_bottom[1]->link}}">
      <img src="{{url('public/img/images_bn/'.$adverts_bottom[1]->img)}}" alt="No image" />
  </a>
  @endif
</div>
<div class="widget text-center">
   @if($adverts_bottom[0]->code != "")
   {{$adverts_bottom[0]->code}}
   @else
   <a href="{{$adverts_bottom[0]->link}}">
      <img src="{{url('public/img/images_bn/'.$adverts_bottom[0]->img)}}" alt="No image" />
  </a>
  @endif
</div>

<!-- Trending news end -->
</div>
<!-- Sidebar right end -->
</div>
            <!-- Sidebar Col end -->